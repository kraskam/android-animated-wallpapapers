package plkraskam.edu.pw.mini.pages.homework.helpers;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.IOException;

import plkraskam.edu.pw.mini.pages.homework.helpers.enums.ImageFilterType;

/**
 * Created by Szprota on 03-May-17.
 */

public class WallpaperController {

    public static void setWallpaper(File file,Context context, ImageFilterType ift){
        Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
        bmp = ImageFilters.bitmapAfterImageEffect(bmp,ift);
        setWallpaper(bmp,context);
    }
    public static void setWallpaper(File file, Context context) {
        Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
        setWallpaper(bmp,context);
    }

    public static void setWallpaper(Bitmap bmp, Context context){
        WallpaperManager m=WallpaperManager.getInstance(context);

        try {
            m.setBitmap(bmp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
