package plkraskam.edu.pw.mini.pages.homework.interfaces;

import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;

/**
 * Created by tweant on 25.04.2017.
 */

public interface IWallpaperClickListener {
    void onClickWallpaper(WallpaperSingleItemModel item);
    void onMoreTopDownloadsClick();
    void onMoreNewestClick();
    void onPlaylistEditClick();
}
