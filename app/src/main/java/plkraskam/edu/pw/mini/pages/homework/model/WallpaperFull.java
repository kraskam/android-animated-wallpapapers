package plkraskam.edu.pw.mini.pages.homework.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WallpaperFull {

    @SerializedName("IsPaid")
    @Expose
    private Boolean isPaid;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("UrlGif")
    @Expose
    private String urlGif;
    @SerializedName("UrlStatic")
    @Expose
    private String urlStatic;
    @SerializedName("DownloadCount")
    @Expose
    private Integer downloadCount;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("UrlFull")
    @Expose
    private String urlFull;

    public Boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlGif() {
        return urlGif;
    }

    public void setUrlGif(String urlGif) {
        this.urlGif = urlGif;
    }

    public String getUrlStatic() {
        return urlStatic;
    }

    public void setUrlStatic(String urlStatic) {
        this.urlStatic = urlStatic;
    }

    public Integer getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(Integer downloadCount) {
        this.downloadCount = downloadCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrlFull() {
        return urlFull;
    }

    public void setUrlFull(String urlFull) {
        this.urlFull = urlFull;
    }

}