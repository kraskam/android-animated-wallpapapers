package plkraskam.edu.pw.mini.pages.homework.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.helpers.constants.Constants;
import plkraskam.edu.pw.mini.pages.homework.helpers.adapters.SectionListDataAdapter;
import plkraskam.edu.pw.mini.pages.homework.helpers.enums.ViewHolderLayoutType;
import plkraskam.edu.pw.mini.pages.homework.interfaces.ISectionListListener;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IWallpaperClickListener;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;
import plkraskam.edu.pw.mini.pages.homework.storage.DbActions;

/**
 * A simple {@link Fragment} subclass.
 */
public class OwnedWallpapersFragment extends Fragment implements ISectionListListener {


    private ArrayList<WallpaperSingleItemModel> ownedList;
    private SectionListDataAdapter ownedAdapter;
    private IWallpaperClickListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_owned_wallpapers, container, false);
        ownedList = DbActions.WallpaperOwned.LoadList(getContext());

        PopulateLists(view);
        SetViewBasedOnPreferences(view);

        return view;
    }

    private void SetViewBasedOnPreferences(View view) {
        TextView info = (TextView) view.findViewById(R.id.owned_wallpapers_info);
        SharedPreferences _preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (_preferences.getBoolean(Constants.SETTING_PLAYLIST_ENABLED, Boolean.FALSE)) {
            info.setText(getResources().getString(R.string.owf_info_playlist));
        } else {
            info.setText(getResources().getString(R.string.owf_info_single));
        }

    }

    private void PopulateLists(View view) {
        RecyclerView ownedRecycler = (RecyclerView) view.findViewById(R.id.owned_wallpapers_recycler_view);
        ownedAdapter = new SectionListDataAdapter(getContext(), ownedList, this, 0, ViewHolderLayoutType.NoPriceSelect);
        LinearLayoutManager freelayoutManager = new GridLayoutManager(getContext(), 3);
        ownedRecycler.setLayoutManager(freelayoutManager);
        ownedRecycler.setAdapter(ownedAdapter);

    }

    @Override
    public void onClickWallpaper(int id, int position) {
        listener.onClickWallpaper(ownedAdapter.getItem(position));
    }

    @Override
    public void onResume() {
        super.onResume();
        ownedAdapter.Refresh();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IWallpaperClickListener) {
            listener = (IWallpaperClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IWallpaperClickListener");
        }
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }
}
