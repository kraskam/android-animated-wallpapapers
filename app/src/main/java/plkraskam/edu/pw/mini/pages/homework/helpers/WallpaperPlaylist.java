package plkraskam.edu.pw.mini.pages.homework.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v4.util.Pair;
import android.support.v7.preference.PreferenceManager;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;

/**
 * Created by Szprota on 10-May-17.
 */

public class WallpaperPlaylist {

    private static Timer timer;
    private static Context context;


    private static List<String> urls = new LinkedList<>();

    private static List<String> names = new LinkedList<>();

    private static int current = 0;

    private static Random generator = new Random();
    private static boolean randomOrder = false;

    public static void initPlaylist(ArrayList<Pair<Long, WallpaperSingleItemModel>> array) {
        if (timer != null)
            timer.cancel();

        int n = array.size();

        for (int i = 0; i < n; i++) {
            urls.add(array.get(i).second.getUrlFull());
            names.add(array.get(i).second.getFileName());
        }
    }

    public static void addNewItemOnPlaylist(WallpaperSingleItemModel item) {
        urls.add(item.getUrlFull());
        urls.add(item.getFileName());

    }

    public static void setRandomOrder(boolean random) {
        randomOrder = random;
        // TODO : MARCIN - wywolywac to tam gdzie sie ustawia random dla playlisty
    }

    public static int fromHoursToMinutes(int hours) {
        return hours * 60;
    }

    private static void nextIndex() {
        if (randomOrder) {
            current = generator.nextInt(urls.size());
        } else {
            current++;
            if (current >= urls.size())
                current = 0;
        }

    }

    public static void startPlaylist(int time, boolean minutes, Context context) {

        if (minutes) {
            time *= 1000 * 60; // from miliseconds to seconds to minutes
        }

        WallpaperPlaylist.context = context;
        if (timer != null)
            timer.cancel();
        timer = new Timer();


        timer.scheduleAtFixedRate(new TimerTask() {


            @Override
            public void run() {
                ImageSaver.saveImage(names.get(current), urls.get(current), WallpaperPlaylist.context, true);
                nextIndex();
            }
        }, 0, time);//put here time 1000 milliseconds=1 second
    }

}
