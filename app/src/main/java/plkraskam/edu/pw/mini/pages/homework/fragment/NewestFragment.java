package plkraskam.edu.pw.mini.pages.homework.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.helpers.constants.Constants;
import plkraskam.edu.pw.mini.pages.homework.helpers.EndlessRecyclerViewScrollListener;
import plkraskam.edu.pw.mini.pages.homework.helpers.adapters.SectionListDataAdapter;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IApiEndpoint;
import plkraskam.edu.pw.mini.pages.homework.interfaces.ISectionListListener;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IWallpaperClickListener;
import plkraskam.edu.pw.mini.pages.homework.model.Datum;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperPaged;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tweant on 24.04.2017.
 */

public class NewestFragment extends Fragment implements ISectionListListener {

    private IWallpaperClickListener listener;
    private ArrayList<WallpaperSingleItemModel> newestList;
    private SectionListDataAdapter newestAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_free_wallpapers, container, false);
        newestList = new ArrayList<>();

        loadNextDataFromApi(1);
        PopulateLists(view);

        return view;
    }

    private void PopulateLists(View view) {
        RecyclerView newestRecycler = (RecyclerView) view.findViewById(R.id.free_wallpapers_recycler_view);
        newestAdapter = new SectionListDataAdapter(getContext(), newestList, this,0);
        LinearLayoutManager freeLayoutManager = new GridLayoutManager(getContext(),3);
        newestRecycler.setLayoutManager(freeLayoutManager);
        newestRecycler.setAdapter(newestAdapter);
        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(freeLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);

            }
        };
        newestRecycler.addOnScrollListener(scrollListener);
        scrollListener.resetState();
    }

    public void loadNextDataFromApi(int offset) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IApiEndpoint apiService = retrofit.create(IApiEndpoint.class);

        Call<WallpaperPaged> newestCall = apiService.getWallpapers(offset, 12, "all","newest");

        newestCall.enqueue(new Callback<WallpaperPaged>() {
            @Override
            public void onResponse(Call<WallpaperPaged> call, Response<WallpaperPaged> response) {
                int statusCode = response.code();
                WallpaperPaged paged = response.body();
                List<WallpaperSingleItemModel> list = new ArrayList<WallpaperSingleItemModel>();
                for (Datum data : paged.getData()) {
                    WallpaperSingleItemModel item = new WallpaperSingleItemModel(data.getId(), data.getName(), data.getDescription(), data.getDownloadCount(), data.getUrlGif(), data.getUrlStatic(),data.getUrlFull());
                    list.add(item);
                }
                newestAdapter.AddRange(list);
            }

            @Override
            public void onFailure(Call<WallpaperPaged> call, Throwable t) {
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getContext(), "Fail while loading data. Check your internet connection.", duration);
                toast.show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IWallpaperClickListener) {
            listener = (IWallpaperClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IWallpaperClickListener");
        }
    }

    @Override
    public void onClickWallpaper(int id, int position) {
        listener.onClickWallpaper(newestAdapter.getItem(position));
    }
}
