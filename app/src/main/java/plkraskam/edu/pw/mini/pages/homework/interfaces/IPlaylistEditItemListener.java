package plkraskam.edu.pw.mini.pages.homework.interfaces;

import android.view.View;

/**
 * Created by tweant on 30.04.2017.
 */

public interface IPlaylistEditItemListener {
    void onRemoveItemClick(View v, int position);
}
