package plkraskam.edu.pw.mini.pages.homework.fragment;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.text.MessageFormat;
import java.util.ArrayList;

import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.helpers.WallpaperPlaylist;
import plkraskam.edu.pw.mini.pages.homework.helpers.adapters.SectionListDataAdapter;
import plkraskam.edu.pw.mini.pages.homework.helpers.constants.Constants;
import plkraskam.edu.pw.mini.pages.homework.helpers.enums.ViewHolderLayoutType;
import plkraskam.edu.pw.mini.pages.homework.interfaces.ISectionListListener;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IWallpaperClickListener;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;
import plkraskam.edu.pw.mini.pages.homework.storage.DbActions;

/**
 * Created by tweant on 24.04.2017.
 */

public class PlaylistFragment extends Fragment implements ISectionListListener {

    private TextView intervalTimeText;
    private IWallpaperClickListener listener;
    private SharedPreferences _preferences;
    private AppCompatTextView pfAutomaticChangeEnabledInfo;
    private AppCompatTextView pfAutomaticChangeDisabledInfo;
    private LinearLayoutCompat playlistLayout;
    private SectionListDataAdapter freeAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlist, container, false);

        InitiateViews(view);
        PopulateLists(view);
        SetIntervalFunctionality(view);
        PopulateButtons(view);

        return view;
    }

    private void InitiateViews(View view) {
        _preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        pfAutomaticChangeEnabledInfo = (AppCompatTextView) view.findViewById(R.id.pfAutomaticChangeEnabledInfo_textView);
        pfAutomaticChangeDisabledInfo = (AppCompatTextView) view.findViewById(R.id.pfAutomaticChangeDisabledInfo_textView);
        playlistLayout = (LinearLayoutCompat) view.findViewById(R.id.playlistLayout);
    }

    private void PopulateButtons(View view) {
        Button editButton = (Button) view.findViewById(R.id.btnEdit);
        ToggleButton randomButton = (ToggleButton) view.findViewById(R.id.random_toggleButton);
        randomButton.setChecked(_preferences.getBoolean(Constants.SETTING_PLAYLIST_RANDOM_ENABLED, Boolean.FALSE));

        ToggleButton playlistEnabledButton = (ToggleButton) view.findViewById(R.id.playlistEnabled_toggleButton);
        playlistEnabledButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            ChangeVisibilityOnPlaylistEnabled(isChecked);
            SharedPreferences.Editor editor = _preferences.edit();
            editor.putBoolean(Constants.SETTING_PLAYLIST_ENABLED, isChecked);
            editor.apply();
        });
        Boolean isPlaylistEnabled = _preferences.getBoolean(Constants.SETTING_PLAYLIST_ENABLED, Boolean.FALSE);
        playlistEnabledButton.setChecked(isPlaylistEnabled);
        ChangeVisibilityOnPlaylistEnabled(isPlaylistEnabled);

        editButton.setOnClickListener(v ->
                listener.onPlaylistEditClick());

        randomButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SharedPreferences.Editor editor = _preferences.edit();
            editor.putBoolean(Constants.SETTING_PLAYLIST_RANDOM_ENABLED, isChecked);
            editor.apply();
        });
    }

    private void SetIntervalFunctionality(View view) {
        Button changeInterval = (Button) view.findViewById(R.id.changeIntervalButton);
        intervalTimeText = (TextView) view.findViewById(R.id.intervalTime);
        int hours = _preferences.getInt(Constants.SETTING_PLAYLIST_HOURS, 12);
        int minutes = _preferences.getInt(Constants.SETTING_PLAYLIST_MINUTES, 0);
        SetIntervalText(intervalTimeText, hours, minutes);

        changeInterval.setOnClickListener(v -> {
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), (timePicker, selectedHour, selectedMinute) ->
            {
                int hour = selectedHour == 0 & selectedMinute == 0 ? 24 : selectedHour;
                SharedPreferences.Editor editor = _preferences.edit();
                editor.putInt(Constants.SETTING_PLAYLIST_HOURS, hour);
                editor.putInt(Constants.SETTING_PLAYLIST_MINUTES, selectedMinute);
                editor.apply();
                SetIntervalText(intervalTimeText, hour, selectedMinute);

                WallpaperPlaylist.startPlaylist(WallpaperPlaylist.fromHoursToMinutes(hour) + minutes, true,
                        getContext());
            }, hours, minutes, true);
            mTimePicker.setTitle(getString(R.string.pf_interval_pop_up_title));
            mTimePicker.show();
        });
    }

    private void PopulateLists(View view) {
        ArrayList<WallpaperSingleItemModel> playlist = DbActions.WallpaperPlaylist.LoadList(getContext());
        RecyclerView freeRecycler = (RecyclerView) view.findViewById(R.id.free_wallpapers_recycler_view);
        freeAdapter = new SectionListDataAdapter(getContext(), playlist, this, 0, ViewHolderLayoutType.NoPrice);
        LinearLayoutManager freeLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        freeRecycler.setLayoutManager(freeLayoutManager);
        freeRecycler.setAdapter(freeAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        freeAdapter.UpdateList(DbActions.WallpaperPlaylist.LoadList(getContext()));
    }

    private void ChangeVisibilityOnPlaylistEnabled(Boolean isChecked) {
        if (isChecked) {
            pfAutomaticChangeEnabledInfo.setVisibility(View.VISIBLE);
            pfAutomaticChangeDisabledInfo.setVisibility(View.GONE);
            playlistLayout.setVisibility(View.VISIBLE);
        } else {
            pfAutomaticChangeEnabledInfo.setVisibility(View.GONE);
            pfAutomaticChangeDisabledInfo.setVisibility(View.VISIBLE);
            playlistLayout.setVisibility(View.GONE);
        }
    }

    private void SetIntervalText(TextView view, int hours, int minutes) {
        String val;
        Object[] args;
        if (hours == 0) {
            val = getResources().getString(R.string.pf_changes_after_minutes);
            args = new Object[]{minutes};
        } else if (minutes == 0) {
            val = getResources().getString(R.string.pf_changes_after_hours);
            args = new Object[]{hours};
        } else {
            val = getResources().getString(R.string.pf_changes_after_full);
            args = new Object[]{hours, minutes};
        }

        MessageFormat fmt = new MessageFormat(val);
        view.setText(fmt.format(args));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IWallpaperClickListener) {
            listener = (IWallpaperClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IWallpaperClickListener");
        }
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

    @Override
    public void onClickWallpaper(int id, int position) {
        listener.onClickWallpaper(freeAdapter.getItem(position));
    }
}
