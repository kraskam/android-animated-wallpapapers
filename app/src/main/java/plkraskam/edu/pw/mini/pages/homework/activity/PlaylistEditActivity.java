package plkraskam.edu.pw.mini.pages.homework.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.fragment.PlaylistEditFragment;
import plkraskam.edu.pw.mini.pages.homework.helpers.Flags;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IPlaylistEditListener;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class PlaylistEditActivity extends AppCompatActivity implements IPlaylistEditListener {

    private FragmentManager _supportFragmentManager;
    private PlaylistEditFragment _playlistEditFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist_edit);

        _supportFragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = _supportFragmentManager.beginTransaction();
        _playlistEditFragment = new PlaylistEditFragment();

        fragmentTransaction.replace(R.id.activity_container, _playlistEditFragment, "PlaylistEdit");
        fragmentTransaction.commit();
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        this.CloseFragment();
    }

    /**
     * Closes fragments and saves data user manipulated.
     */
    @Override
    public void CloseFragment() {
        _playlistEditFragment.SaveListToDatabase();
        _playlistEditFragment.initPlaylist();
        Intent returnIntent = NavUtils.getParentActivityIntent(this);
        returnIntent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        NavUtils.navigateUpTo(this, returnIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.CloseFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Closes fragment and redirects user to landing page. This is not addiditonal idem adding functionality.
     */
    @Override
    public void AddItemCloseFragment() {
        _playlistEditFragment.SaveListToDatabase(); // user may have changed something before deciding to add new wallpaper
        Intent returnIntent = new Intent();
        returnIntent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        setResult(Flags.RESULT_GO_MAIN_FRAGMENT,returnIntent);
        finish();
    }


}
