package plkraskam.edu.pw.mini.pages.homework.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.fragment.NewestFragment;
import plkraskam.edu.pw.mini.pages.homework.fragment.OwnedWallpapersFragment;
import plkraskam.edu.pw.mini.pages.homework.fragment.TopDownloadsFragment;
import plkraskam.edu.pw.mini.pages.homework.fragment.PlaylistFragment;
import plkraskam.edu.pw.mini.pages.homework.fragment.WallpaperLandingPageFragment;
import plkraskam.edu.pw.mini.pages.homework.helpers.constants.Constants;
import plkraskam.edu.pw.mini.pages.homework.helpers.Flags;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IApiEndpoint;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IWallpaperClickListener;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements IWallpaperClickListener {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg;
    private TextView txtName;
    private Toolbar toolbar;
    private WallpaperLandingPageFragment mainFragment;

    // urls to load navigation header background image
    // and profile image
    private static final String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_SETTINGS = "settings";
    private static final String TAG_NEWEST = "newest";
    private static final String TAG_TOP_DOWNLOADS = "top_downloads";
    private static final String TAG_OWNED_WALLPAPERS = "owned_wallpapers";
    private static final String TAG_PLAYLIST = "playlist";
    public static String CURRENT_TAG = TAG_HOME;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Fresco.initialize(getApplicationContext());
        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);

        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);


        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);


        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }


    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        // name, website
        txtName.setText(getString(R.string.nav_title));


        // loading header background image
        Glide.with(this).load(urlNavHeaderBg)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);


        // showing dot next to notifications label
        //navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button

            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = () -> {
            // update the main content by replacing fragments
            Fragment fragment = getHomeFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                    android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
            fragmentTransaction.commitAllowingStateLoss();
        };

        // If mPendingRunnable is not null, then add to the message queue
        mHandler.post(mPendingRunnable);

        // show or hide the fab button

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                mainFragment = new WallpaperLandingPageFragment();
                return mainFragment;
            case 1:
                return new NewestFragment();
            case 2:
                return new TopDownloadsFragment();
            case 3:
                return new OwnedWallpapersFragment();
            case 4:
                return new PlaylistFragment();
            case 5:
                // settings fragment
                return new PrefsFragment();
            default:
                mainFragment = new WallpaperLandingPageFragment();
                return mainFragment;
        }
    }

    private void setToolbarTitle() {
        try {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(activityTitles[navItemIndex]);
            }
        } catch (NullPointerException exc) {
            exc.printStackTrace();
        }

    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(menuItem -> {

            //Check to see which item was being clicked and perform appropriate action
            switch (menuItem.getItemId()) {
                //Replacing the main content with ContentFragment Which is our Inbox View;
                case R.id.nav_home:
                    navItemIndex = 0;
                    CURRENT_TAG = TAG_HOME;
                    break;
                case R.id.nav_newest:
                    navItemIndex = 1;
                    CURRENT_TAG = TAG_NEWEST;
                    break;
                case R.id.nav_top_downloads:
                    navItemIndex = 2;
                    CURRENT_TAG = TAG_TOP_DOWNLOADS;
                    break;
                case R.id.nav_my_wallpapers:
                    navItemIndex = 3;
                    CURRENT_TAG = TAG_OWNED_WALLPAPERS;
                    break;
                case R.id.nav_playlist:
                    navItemIndex = 4;
                    CURRENT_TAG = TAG_PLAYLIST;
                    break;
                case R.id.nav_settings:
                    navItemIndex = 5;
                    CURRENT_TAG = TAG_SETTINGS;
                    break;
                default:
                    navItemIndex = 0;
            }

            //Checking if the item is in checked state or not, if not make it in checked state
            if (menuItem.isChecked()) {
                menuItem.setChecked(false);
            } else {
                menuItem.setChecked(true);
            }
            menuItem.setChecked(true);

            loadHomeFragment();

            return true;
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.addDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    public void onClickWallpaper(WallpaperSingleItemModel item) {
        Intent intent = new Intent(getApplicationContext(), WallpaperPreviewActivity.class);
        intent.putExtra("wallpaper", item);
        startActivityForResult(intent, Flags.REQUEST_CODE_WALLPAPER_PREVIEW);
        //Toast.makeText(getApplicationContext(), "[DEV] Opened wallpaper info" , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMoreTopDownloadsClick() {
        navItemIndex = 2;
        CURRENT_TAG = TAG_TOP_DOWNLOADS;
        loadHomeFragment();
    }

    @Override
    public void onMoreNewestClick() {
        navItemIndex = 1;
        CURRENT_TAG = TAG_NEWEST;
        loadHomeFragment();
    }

    @Override
    public void onPlaylistEditClick() {
        Intent intent = new Intent(getApplicationContext(), PlaylistEditActivity.class);
        startActivityForResult(intent, Flags.REQUEST_CODE_PLAYLIST_EDIT);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Flags.REQUEST_CODE_WALLPAPER_PREVIEW && resultCode == Flags.RESULT_OK_RETURN_PARENT) {
        } else if (requestCode == Flags.REQUEST_CODE_PLAYLIST_EDIT) {
            if (resultCode == Flags.RESULT_OK_RETURN_PARENT) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();

                navItemIndex = 4;
                CURRENT_TAG = TAG_PLAYLIST;
                loadHomeFragment();
            } else if (resultCode == Flags.RESULT_GO_MAIN_FRAGMENT) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
            }
        } else if (requestCode == Flags.REQUEST_CODE_WALLPAPER_UPLOAD_TEXT & resultCode == RESULT_OK) {
            filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);

            name = null;
            description = null;
            showInputDialog();

        }

    }


    private String name;
    private String description;
    private String filePath;

    protected void showInputDialog() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editName = (EditText) promptView.findViewById(R.id.nameEdit);
        final EditText editDescription = (EditText) promptView.findViewById(R.id.descriptionEdit);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> {
                    name = editName.getText().toString();
                    description = editDescription.getText().toString();
                    if (name != null & description != null) {

                    }
                    OkHttpClient client = new OkHttpClient.Builder().readTimeout(0, TimeUnit.SECONDS)
                            .connectTimeout(0, TimeUnit.SECONDS)
                            .build();
                    // Change base URL to your upload server URL.
                    IApiEndpoint service = new Retrofit.Builder().baseUrl(Constants.API_URL).addConverterFactory(GsonConverterFactory.create()).client(client).build().create(IApiEndpoint.class);

                    File file = new File(filePath);

                    RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                    MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
                    RequestBody name_r = RequestBody.create(MediaType.parse("text/plain"), "upload_test");

                    Toast.makeText(getApplicationContext(), "Please be patient. It may take a while depending on your file size.", Toast.LENGTH_LONG).show();
                    retrofit2.Call<okhttp3.ResponseBody> req = service.postImage(body, name, description);
                    req.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Toast.makeText(getApplicationContext(), "Upload successful", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Error while uploading", Toast.LENGTH_SHORT).show();
                            t.printStackTrace();

                        }
                    });

                })
                .setNegativeButton("Cancel",
                        (dialog, id) -> dialog.cancel());

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    public static class PrefsFragment extends PreferenceFragmentCompat {

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.preferences, rootKey);
        }
    }
}
