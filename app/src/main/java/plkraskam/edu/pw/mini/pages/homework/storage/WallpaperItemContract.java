package plkraskam.edu.pw.mini.pages.homework.storage;

import android.provider.BaseColumns;

/**
 * Created by tweant on 01.05.2017.
 */

public class WallpaperItemContract {
    private WallpaperItemContract() {
    }

    public static class WallpaperPlaylistEntry implements BaseColumns {
        public static final String TABLE_NAME = "playlist";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_IMAGEURL = "url";
        public static final String COLUMN_IMAGEURL_SMALL_GIF = "url_small_gif";
        public static final String COLUMN_IMAGEURL_SMALL_STATIC = "url_small_static";
        public static final String COLUMN_DOWNLOADED_COUNT = "downloaded_count";
    }

    public static class WallpaperOwnedEntry implements BaseColumns {
        public static final String TABLE_NAME = "owned";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_SELECTED = "selected";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_IMAGEURL = "url";
        public static final String COLUMN_IMAGEURL_SMALL_GIF = "url_small_gif";
        public static final String COLUMN_IMAGEURL_SMALL_STATIC = "url_small_static";
        public static final String COLUMN_DOWNLOADED_COUNT = "downloaded_count";
    }
}
