package plkraskam.edu.pw.mini.pages.homework.helpers;

import android.graphics.Bitmap;
import android.graphics.Color;

import plkraskam.edu.pw.mini.pages.homework.helpers.enums.ImageFilterType;

/**
 * Created by Szprota on 04-May-17.
 */

public class ImageFilters {

    public static Bitmap bitmapAfterImageEffect(Bitmap bitmap, ImageFilterType ift){
        if(ift == ImageFilterType.BLACK_AND_WHITE){
            return blackAndWhite(bitmap);
        }else if(ift == ImageFilterType.EDGE_DETECTION){
            return edgeDetection(bitmap);
        }else if(ift == ImageFilterType.INVERTED_COLORS) {
            return invertedColorsFilter(bitmap);
        }else if(ift == ImageFilterType.RED_CHANNEL){
            return redChannel(bitmap);
        }// if NONE
        return bitmap;
    }

    private static int[] getPixels(Bitmap bmp){
        int[] pixels = new int[bmp.getHeight()*bmp.getWidth()];
        bmp.getPixels(pixels, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        return pixels;
    }

    public static Bitmap getMutableBitmap(Bitmap bmp){
        Bitmap.Config config = bmp.getConfig() != null ? bmp.getConfig() : Bitmap.Config.ARGB_8888;
        return bmp.copy(config,true);
    }

    public static Bitmap invertedColorsFilter(Bitmap myBitmap){

        myBitmap = getMutableBitmap(myBitmap);
        int n = myBitmap.getWidth()*myBitmap.getHeight();
        int[] pixels = getPixels(myBitmap);
        int[] newPixels = new int[n];
        for (int i=0; i<n; i++){
            int redValue = Color.red(pixels[i]);
            int blueValue = Color.blue(pixels[i]);
            int greenValue = Color.green(pixels[i]);
            newPixels[i] = Color.rgb(255 - redValue, 255 - greenValue, 255 - blueValue);
        }

        myBitmap.setPixels(newPixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
        return myBitmap;
    }

    public static Bitmap blackAndWhite(Bitmap myBitmap){

        myBitmap = getMutableBitmap(myBitmap);
        int n = myBitmap.getWidth()*myBitmap.getHeight();
        int[] pixels = getPixels(myBitmap);
        int[] newPixels = new int[n];
        for (int i=0; i<n; i++){
            int redValue = Color.red(pixels[i]);
            int blueValue = Color.blue(pixels[i]);
            int greenValue = Color.green(pixels[i]);
            double lum = 0.299 * redValue + 0.7152 * greenValue + 0.0722*blueValue;
            int l = (int)lum;
            newPixels[i] = Color.rgb(l,l,l);
        }

        myBitmap.setPixels(newPixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
        return myBitmap;
    }


    public static Bitmap redChannel(Bitmap myBitmap){

        myBitmap = getMutableBitmap(myBitmap);
        int n = myBitmap.getWidth()*myBitmap.getHeight();
        int[] pixels = getPixels(myBitmap);
        int[] newPixels = new int[n];
        for (int i=0; i<n; i++){
            int r = Color.red(pixels[i]);

            newPixels[i] = Color.rgb(r,r/2,r/4);
        }

        myBitmap.setPixels(newPixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
        return myBitmap;
    }

    public static Bitmap edgeDetection(Bitmap myBitmap){

        myBitmap = getMutableBitmap(myBitmap);
        int n = myBitmap.getWidth()*myBitmap.getHeight();
        int[] pixels = getPixels(myBitmap);
        int[] newPixels = new int[n];
        int width = myBitmap.getWidth();
        for (int i=0; i<n; i++){
            int redValue = Color.red(pixels[i]);
            int blueValue = Color.blue(pixels[i]);
            int greenValue = Color.green(pixels[i]);
            if(i % myBitmap.getWidth() != 0 && i-1- width > 0){
                redValue -= Color.red(pixels[i-1 - width]);
                blueValue -= Color.blue(pixels[i-1 - width]);
                greenValue -= Color.green(pixels[i-1 - width]);
            }

            newPixels[i] = Color.rgb(redValue, greenValue, blueValue);
        }

        myBitmap.setPixels(newPixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
        return myBitmap;
    }
}
