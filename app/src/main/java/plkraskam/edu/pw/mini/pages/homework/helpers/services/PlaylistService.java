package plkraskam.edu.pw.mini.pages.homework.helpers.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import plkraskam.edu.pw.mini.pages.homework.helpers.WallpaperPlaylist;

/**
 * Created by Szprota on 20-May-17.
 */

public class PlaylistService extends Service {

    private static Intent serviceIntent;
    private static Context context;
    public static void startPlaylistService(int time, boolean minutes, Context context){

        PlaylistService.context = context;
        serviceIntent = new Intent(context,PlaylistService.class);
        serviceIntent.putExtra("minutes", minutes);
        serviceIntent.putExtra("time", time);
        PlaylistService.context.startService(serviceIntent);
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Bundle extras = intent.getExtras();

        boolean minutes = (boolean) extras.get("minutes");
        int time = (int) extras.get("time");

        WallpaperPlaylist.startPlaylist(time,minutes,context);

        return super.onStartCommand(intent, flags, startId);
    }
}
