package plkraskam.edu.pw.mini.pages.homework.helpers;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Szprota on 14-May-17.
 */

public class PixelAnimationsController {


    private static Context context;
    private static Timer timer = null;
    private static int animationProgress = 0; // [0,100]
    private static Bitmap startingBitmap = null;

    public static void StartAnimation(Context context){
        PixelAnimationsController.context = context;

        startingBitmap = getCurrentWallpaper();

        if (timer != null)
            timer.cancel();
        timer = new Timer();


        timer.scheduleAtFixedRate(new TimerTask() {


            @Override
            public void run() {
                TestAnimation();
                animationProgress = (animationProgress + 1) % 100;
            }
        }, 0, 800);//put here time 1000 milliseconds=1 second
    }

    private static int[] getPixels(Bitmap bmp){
        int[] pixels = new int[bmp.getHeight()*bmp.getWidth()];
        bmp.getPixels(pixels, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        return pixels;
    }

    public static void TestAnimation(){
        //if(startingBitmap == null){

       // }
        Bitmap bitmap = startingBitmap;
        bitmap = getMutableBitmap(bitmap);
        int n = bitmap.getWidth()*bitmap.getHeight();
        int[] pixels = getPixels(bitmap);
        int[] newPixels = new int[n];

        int width = bitmap.getWidth();
        int w = (int)(width * (float)animationProgress/100.0f + 1);
        int animationWidth = 20;
        for (int i=0; i<n; i++){
            int redValue = Color.red(pixels[i]);
            int blueValue = Color.blue(pixels[i]);
            int greenValue = Color.green(pixels[i]);

            int index = i % (width+1);
            if(index <= w + animationWidth &&  index >= w - animationWidth){

                blueValue = redValue;
                greenValue = redValue;
            }


            newPixels[i] = Color.rgb(redValue, greenValue, blueValue);
        }

        bitmap.setPixels(newPixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        WallpaperController.setWallpaper(bitmap,context);
    }



    public static Bitmap getMutableBitmap(Bitmap bmp){
        Bitmap.Config config = bmp.getConfig() != null ? bmp.getConfig() : Bitmap.Config.ARGB_8888;
        return bmp.copy(config,true);
    }

    private static Bitmap getCurrentWallpaper(){
        final WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
        final Drawable wallpaperDrawable = wallpaperManager.getDrawable();
        return drawableToBitmap(wallpaperDrawable);

    }

    private static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
