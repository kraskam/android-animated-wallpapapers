package plkraskam.edu.pw.mini.pages.homework.helpers.toasts;

import android.content.Context;
import android.widget.Toast;

import plkraskam.edu.pw.mini.pages.homework.R;

public class Toasts {
    public static void FailLoadingDataCheckConnection(Context context) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, context.getString(R.string.pwf_error_internet), duration);
        toast.show();
    }

    public static void PlaylistNotEnabled(Context context) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, context.getString(R.string.t_playlist_not_enabled), duration);
        toast.show();
    }

    public static void PlaylistSuccessfullyAdded(Context context) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, context.getString(R.string.t_playlist_successfully_added), duration);
        toast.show();
    }
}
