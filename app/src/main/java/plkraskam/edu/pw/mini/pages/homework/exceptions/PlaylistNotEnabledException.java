package plkraskam.edu.pw.mini.pages.homework.exceptions;

/**
 * Created by tweant on 09.05.2017.
 */

public class PlaylistNotEnabledException extends Exception {
    public PlaylistNotEnabledException() {
        super("Enable playlist functionality first.");
    }

    public PlaylistNotEnabledException(String message) {
        super(message);
    }
}
