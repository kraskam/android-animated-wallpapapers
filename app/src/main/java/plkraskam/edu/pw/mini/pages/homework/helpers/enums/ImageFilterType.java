package plkraskam.edu.pw.mini.pages.homework.helpers.enums;

/**
 * Created by Szprota on 06-May-17.
 */

public enum ImageFilterType {
    NONE,
    INVERTED_COLORS,
    BLACK_AND_WHITE,
    RED_CHANNEL,
    EDGE_DETECTION
}
