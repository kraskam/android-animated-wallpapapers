package plkraskam.edu.pw.mini.pages.homework.helpers.enums;

/**
 * Created by tweant on 03.05.2017.
 */

public enum ViewHolderLayoutType {
    Full,
    NoPrice,
    NoPriceSelect
}
