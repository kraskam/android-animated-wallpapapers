package plkraskam.edu.pw.mini.pages.homework.helpers;

import java.io.File;

import plkraskam.edu.pw.mini.pages.homework.helpers.enums.ImageFilterType;

/**
 * Created by Szprota on 06-May-17.
 */

public class SavingFile {

    public File file;
    public boolean toWallpaper = false;
    public ImageFilterType ift;

    public SavingFile(File file, boolean wall, ImageFilterType i ){
        this.file = file;
        toWallpaper = wall;
        ift = i;
    }

}
