package plkraskam.edu.pw.mini.pages.homework.model;

import android.os.Parcel;
import android.os.Parcelable;


public class WallpaperSingleItemModel implements Parcelable {

    private String name;
    private String urlFull;
    private String urlGif;
    private String urlStatic;
    private String description;
    private int downloadCount;
    private int ID;
    private boolean selected;

    public WallpaperSingleItemModel(int ID, String name, String description, int downloadCount, String urlGif, String urlStatic, String urlFull) {
        this.name = name;
        this.urlGif = urlGif;
        this.urlStatic = urlStatic;
        this.description = description;
        this.downloadCount=downloadCount;
        this.urlFull=urlFull;
        this.ID=ID;
    }


    public String getFileName(){
        return name + "."+ urlFull.substring(urlFull.lastIndexOf(".") + 1);
    }

    public Boolean isSelected() {return selected;}


    public void setSelected() {this.selected = true;}

    public void setSelected(Boolean selected) {this.selected = selected;}

    /**
     * Returns full GIF for wallpaper setting.
     * @return Full GIF
     */
    public String getUrlFull() {
        return urlFull;
    }

    /**
     * Returns GIF image only for displaying within list.
     * @return 200x200 GIF image
     */
    public String getUrlGif() {return  urlGif;}

    /**
     * Returns image only for displaying within a list when user doesn't want to display GIFs.
     * @return 200x200 static image
     */
    public String getUrlStatic() {return urlStatic;}

    public int getDownloadCount() {return downloadCount;}

    public String getName() {
        return name;
    }

    public int getID() { return ID;}

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeString(name);
        dest.writeInt(downloadCount);
        dest.writeString(urlFull);
        dest.writeString(urlGif);
        dest.writeString(urlStatic);
        dest.writeString(description);

    }

    public static final Parcelable.Creator<WallpaperSingleItemModel> CREATOR = new Parcelable.Creator<WallpaperSingleItemModel>() {
        public WallpaperSingleItemModel createFromParcel(Parcel in) {
            return new WallpaperSingleItemModel(in);
        }

        public WallpaperSingleItemModel[] newArray(int size) {
            return new WallpaperSingleItemModel[size];
        }
    };

    private WallpaperSingleItemModel(Parcel in) {
        ID = in.readInt();
        name = in.readString();
        downloadCount = in.readInt();
        urlFull = in.readString();
        urlGif=in.readString();
        urlStatic=in.readString();
        description = in.readString();
    }
}
