package plkraskam.edu.pw.mini.pages.homework.helpers.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.helpers.constants.Constants;
import plkraskam.edu.pw.mini.pages.homework.helpers.enums.ViewHolderLayoutType;
import plkraskam.edu.pw.mini.pages.homework.interfaces.ISectionListListener;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;

import static java.lang.Boolean.FALSE;

/**
 * Created by tweant on 24.04.2017.
 */

public class SectionListDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    class ViewHolderFull extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTitle;
        TextView price;
        SimpleDraweeView itemImage;

        ViewHolderFull(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.price = (TextView) view.findViewById(R.id.price);
            this.itemImage = (SimpleDraweeView) view.findViewById(R.id.itemImage);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClickWallpaper(id, this.getLayoutPosition());
        }

    }


    class ViewHolderNoPriceSelected extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTitle;
        SimpleDraweeView itemImage;
        ImageView selectCircle;
        ImageView selectIcon;

        ViewHolderNoPriceSelected(View view) {
            super(view);
            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (SimpleDraweeView) view.findViewById(R.id.itemImage);
            this.selectCircle = (ImageView) view.findViewById(R.id.select_circle);
            this.selectIcon = (ImageView) view.findViewById(R.id.select_icon);


            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClickWallpaper(id, this.getLayoutPosition());
        }

    }

    class ViewHolderNoPrice extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTitle;
        SimpleDraweeView itemImage;

        ViewHolderNoPrice(View view) {
            super(view);
            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (SimpleDraweeView) view.findViewById(R.id.itemImage);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClickWallpaper(id, this.getLayoutPosition());
        }

    }

    private ArrayList<WallpaperSingleItemModel> itemsList;
    private Context mContext;
    private static ISectionListListener listener;
    private int id;
    private ViewHolderLayoutType type;

    public SectionListDataAdapter(Context context, ArrayList<WallpaperSingleItemModel> itemsList, ISectionListListener listener, int id) {
        this.itemsList = itemsList;
        this.mContext = context;
        SectionListDataAdapter.listener = listener;
        this.id = id;
        this.type = ViewHolderLayoutType.Full;
    }

    public SectionListDataAdapter(Context context, ArrayList<WallpaperSingleItemModel> itemsList, ISectionListListener listener, int id, ViewHolderLayoutType type) {
        this.itemsList = itemsList;
        this.mContext = context;
        SectionListDataAdapter.listener = listener;
        this.id = id;
        this.type = type;
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View listView;
        switch (type) {
            case Full:
                listView = inflater.inflate(R.layout.list_single_card, parent, false);
                return new ViewHolderFull(listView);
            case NoPrice:
                listView = inflater.inflate(R.layout.list_single_card_no_price, parent, false);
                return new ViewHolderNoPrice(listView);
            case NoPriceSelect:
                listView = inflater.inflate(R.layout.list_single_card_no_price_selected, parent, false);
                return new ViewHolderNoPriceSelected(listView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        WallpaperSingleItemModel singleItem = itemsList.get(position);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        Boolean showDynamic = preferences.getBoolean("wallpapers_show_dynamic", FALSE);
        switch (type) {
            case Full:
                ViewHolderFull viewHolder = (ViewHolderFull) holder;
                viewHolder.tvTitle.setText(singleItem.getName());
                viewHolder.price.setText(String.valueOf(singleItem.getDownloadCount()));
                if (showDynamic) {
//                    Glide.with(getContext())
//                            .load(singleItem.getUrlGif())
//                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                            .centerCrop()
//                            .into(viewHolder.itemImage);
                    Uri uri = Uri.parse(singleItem.getUrlGif());
                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setUri(uri)
                            .setAutoPlayAnimations(true)
                            .build();
                    viewHolder.itemImage.setController(controller);
                } else {
//                    Glide.with(getContext())
//                            .load(singleItem.getUrlStatic())
//                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                            .centerCrop()
//                            .into(viewHolder.itemImage);
                    Uri uri = Uri.parse(singleItem.getUrlStatic());
                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setUri(uri)
                            .setAutoPlayAnimations(true)
                            .build();
                    viewHolder.itemImage.setController(controller);
                }
                break;
            case NoPrice:
                ViewHolderNoPrice viewHolderNoPrice = (ViewHolderNoPrice) holder;
                viewHolderNoPrice.tvTitle.setText(singleItem.getName());

                if (showDynamic) {
//                    Glide.with(getContext())
//                            .load(singleItem.getUrlGif())
//                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                            .centerCrop()
//                            .into(viewHolderNoPrice.itemImage);
                    Uri uri = Uri.parse(singleItem.getUrlGif());
                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setUri(uri)
                            .setAutoPlayAnimations(true)
                            .build();
                    viewHolderNoPrice.itemImage.setController(controller);
                } else {
//                    Glide.with(getContext())
//                            .load(singleItem.getUrlStatic())
//                            .asBitmap()
//                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                            .centerCrop()
//                            .into(viewHolderNoPrice.itemImage);
                    Uri uri = Uri.parse(singleItem.getUrlStatic());
                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setUri(uri)
                            .setAutoPlayAnimations(true)
                            .build();
                    viewHolderNoPrice.itemImage.setController(controller);
                }
                break;
            case NoPriceSelect:
                ViewHolderNoPriceSelected viewHolderNoPriceSelected = (ViewHolderNoPriceSelected) holder;
                viewHolderNoPriceSelected.tvTitle.setText(singleItem.getName());
                SharedPreferences _preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                if(singleItem.isSelected() && !_preferences.getBoolean(Constants.SETTING_PLAYLIST_ENABLED, Boolean.FALSE))
                {
                    viewHolderNoPriceSelected.selectIcon.setVisibility(View.VISIBLE);
                    viewHolderNoPriceSelected.selectCircle.setVisibility(View.VISIBLE);
                }

                if (showDynamic) {
//                    Glide.with(getContext())
//                            .load(singleItem.getUrlGif())
//                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                            .centerCrop()
//                            .into(viewHolderNoPriceSelected.itemImage);
                    Uri uri = Uri.parse(singleItem.getUrlGif());
                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setUri(uri)
                            .setAutoPlayAnimations(true)
                            .build();
                    viewHolderNoPriceSelected.itemImage.setController(controller);
                } else {
//                    Glide.with(getContext())
//                            .load(singleItem.getUrlStatic())
//                            .asBitmap()
//                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                            .centerCrop()
//                            .into(viewHolderNoPriceSelected.itemImage);
                    Uri uri = Uri.parse(singleItem.getUrlStatic());
                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setUri(uri)
                            .setAutoPlayAnimations(true)
                            .build();
                    viewHolderNoPriceSelected.itemImage.setController(controller);
                }
                break;
        }
    }

    public void UpdateList(ArrayList<WallpaperSingleItemModel> newlist) {
        if (itemsList != null) {
            itemsList.clear();
            itemsList.addAll(newlist);
        }
        this.notifyDataSetChanged();
    }

    public void Refresh()
    {
        this.notifyDataSetChanged();
    }

    public void AddToList(WallpaperSingleItemModel item) {
        itemsList.add(item);
        this.notifyDataSetChanged();
    }

    public void AddRange(List<WallpaperSingleItemModel> list) {
        int count = getItemCount();
        itemsList.addAll(list);
        this.notifyItemRangeInserted(count,getItemCount()-1);
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public WallpaperSingleItemModel getItem(int position) {
        return itemsList.get(position);
    }


}
