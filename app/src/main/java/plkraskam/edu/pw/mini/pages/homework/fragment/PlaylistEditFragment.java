package plkraskam.edu.pw.mini.pages.homework.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.woxthebox.draglistview.DragListView;

import java.util.ArrayList;

import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.helpers.WallpaperPlaylist;
import plkraskam.edu.pw.mini.pages.homework.helpers.adapters.PlaylistStableAdapter;
import plkraskam.edu.pw.mini.pages.homework.helpers.adapters.WallpaperPlaylistDragItem;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IPlaylistEditItemListener;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IPlaylistEditListener;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;
import plkraskam.edu.pw.mini.pages.homework.storage.DbActions;


public class PlaylistEditFragment extends Fragment implements IPlaylistEditItemListener {

    private IPlaylistEditListener listener;
    private ArrayList<Pair<Long, WallpaperSingleItemModel>> _playlist;
    private PlaylistStableAdapter listAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _playlist = DbActions.WallpaperPlaylist.LoadListForDragging(getContext());
        initPlaylist();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlist_edit, container, false);

        PopulateLists(view);
        PopulateButtons(view);

        return view;
    }


    public void initPlaylist() {
        WallpaperPlaylist.initPlaylist(_playlist);
    }

    private void PopulateButtons(View view) {
        FloatingActionButton addButton = (FloatingActionButton) view.findViewById(R.id.floating_add);
        addButton.setOnClickListener(v -> listener.AddItemCloseFragment());
    }

    private void PopulateLists(View view) {
        DragListView mDragListView = (DragListView) view.findViewById(R.id.drag_list_view);
        mDragListView.getRecyclerView().setVerticalScrollBarEnabled(true);
        mDragListView.setScrollingEnabled(true);
        mDragListView.setCustomDragItem(new WallpaperPlaylistDragItem(getContext(), R.layout.list_item));

        mDragListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        listAdapter = new PlaylistStableAdapter(getContext(), _playlist, R.layout.list_item, R.id.itemImage, false, this);
        mDragListView.setAdapter(listAdapter, true);
        mDragListView.setCanDragHorizontally(false);
    }

    @Override
    public void onRemoveItemClick(View v, int position) {
        _playlist.remove(position);
        listAdapter.notifyItemRemoved(position);
    }

    public void SaveListToDatabase() {
        DbActions.WallpaperPlaylist.SaveList(getContext(), _playlist);
        initPlaylist();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IPlaylistEditListener) {
            listener = (IPlaylistEditListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnItemAddFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

}
