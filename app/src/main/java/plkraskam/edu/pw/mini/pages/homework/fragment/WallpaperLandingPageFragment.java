package plkraskam.edu.pw.mini.pages.homework.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nbsp.materialfilepicker.MaterialFilePicker;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.helpers.Flags;
import plkraskam.edu.pw.mini.pages.homework.helpers.adapters.SectionListDataAdapter;
import plkraskam.edu.pw.mini.pages.homework.helpers.constants.Constants;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IApiEndpoint;
import plkraskam.edu.pw.mini.pages.homework.interfaces.ISectionListListener;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IWallpaperClickListener;
import plkraskam.edu.pw.mini.pages.homework.model.Datum;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperPaged;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WallpaperLandingPageFragment extends Fragment implements ISectionListListener {

    private IWallpaperClickListener listener;

    ArrayList<SectionListDataAdapter> adapters;
    RecyclerView topDownloads;
    RecyclerView newest;
    ArrayList<WallpaperSingleItemModel> freeList;
    ArrayList<WallpaperSingleItemModel> paidList;
    private int loaded = 0;
    LinearLayout loadingView;
    LinearLayout recycleViews;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallpaper_landing_page, container, false);

        ImportWallpapers();
        InitiateViews(view);
        PopulateLists(view);
        PopulateButtons(view);

        return view;
    }

    private void InitiateViews(View view) {
        loadingView = (LinearLayout) view.findViewById(R.id.loadingView);
        recycleViews = (LinearLayout) view.findViewById(R.id.recycleViews);
    }

    private void PopulateButtons(View view) {
        Button topDownloadsMoreButton = (Button) view.findViewById(R.id.btnFreeMore);
        Button newestMoreButton = (Button) view.findViewById(R.id.btnPaidMore);
        topDownloadsMoreButton.setOnClickListener(v -> listener.onMoreTopDownloadsClick());
        newestMoreButton.setOnClickListener(v -> listener.onMoreNewestClick());
        Button UploadFromFilesButton = (Button) view.findViewById(R.id.btnUploadFromFiles);
        UploadFromFilesButton.setOnClickListener(v -> checkPermissionsAndOpenFilePicker());
    }

    private void PopulateLists(View view) {
        adapters = new ArrayList<>();
        topDownloads = (RecyclerView) view.findViewById(R.id.free_wallpapers_recycler_view);
        SectionListDataAdapter topDownloadsAdapter = new SectionListDataAdapter(getContext(), freeList, this, 0);
        adapters.add(topDownloadsAdapter);
        LinearLayoutManager freeLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        topDownloads.setLayoutManager(freeLayoutManager);
        topDownloads.setAdapter(topDownloadsAdapter);

        newest = (RecyclerView) view.findViewById(R.id.paid_wallpapers_recycler_view);
        SectionListDataAdapter newestAdapter = new SectionListDataAdapter(getContext(), paidList, this, 1);
        adapters.add(newestAdapter);
        LinearLayoutManager paidLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        newest.setLayoutManager(paidLayoutManager);
        newest.setAdapter(newestAdapter);
    }

    void openFilePicker()
    {
        new MaterialFilePicker()
                .withActivity(getActivity())
                .withRequestCode(Flags.REQUEST_CODE_WALLPAPER_UPLOAD_TEXT)
                .withFilter(Pattern.compile(".*\\.gif$")) // Filtering files and directories by file name using regexp
                .withFilterDirectories(false) // Set directories filterable (false by default)
                .withHiddenFiles(true) // Show hidden files and folders
                .start();
    }

    private void checkPermissionsAndOpenFilePicker() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                showError();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, Flags.REQUEST_CODE_WALLPAPER_UPLOAD_TEXT);
            }
        } else {
            openFilePicker();
        }
    }

    private void showError() {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(getContext(), getString(R.string.wlpg_storage_permission), duration);
        toast.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case Flags.REQUEST_CODE_WALLPAPER_UPLOAD_TEXT: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker();
                } else {
                    showError();
                }
            }
        }
    }

    public void ImportWallpapers() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IApiEndpoint apiService = retrofit.create(IApiEndpoint.class);

        Call<WallpaperPaged> topDownloadsCall = apiService.getWallpapers(1, 10, "all","top_downloads");
        topDownloadsCall.enqueue(new Callback<WallpaperPaged>() {
            @Override
            public void onResponse(Call<WallpaperPaged> call, Response<WallpaperPaged> response) {
                WallpaperPaged paged = response.body();
                List<WallpaperSingleItemModel> list = new ArrayList<>();
                for (Datum data : paged.getData()) {
                    WallpaperSingleItemModel item = new WallpaperSingleItemModel(data.getId(), data.getName(), data.getDescription(), data.getDownloadCount(), data.getUrlGif(), data.getUrlStatic(),data.getUrlFull());
                    list.add(item);
                }
                adapters.get(0).AddRange(list);
                CheckVisibility();
            }

            @Override
            public void onFailure(Call<WallpaperPaged> call, Throwable t) {
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getContext(), getString(R.string.pwf_error_internet), duration);
                toast.show();
            }
        });

        freeList = new ArrayList<>();
        paidList = new ArrayList<>();

        Call<WallpaperPaged> newestWallpapersCall = apiService.getWallpapers(1, 10, "all","newest");
        newestWallpapersCall.enqueue(new Callback<WallpaperPaged>() {
            @Override
            public void onResponse(Call<WallpaperPaged> call, Response<WallpaperPaged> response) {
                WallpaperPaged paged = response.body();
                List<WallpaperSingleItemModel> list = new ArrayList<>();
                for (Datum data : paged.getData()) {
                    WallpaperSingleItemModel item = new WallpaperSingleItemModel(data.getId(), data.getName(), data.getDescription(), data.getDownloadCount(), data.getUrlGif(), data.getUrlStatic(),data.getUrlFull());
                    list.add(item);
                }
                adapters.get(1).AddRange(list);
                CheckVisibility();
            }

            @Override
            public void onFailure(Call<WallpaperPaged> call, Throwable t) {
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getContext(), getString(R.string.pwf_error_internet), duration);
                toast.show();
            }
        });

    }

    private void CheckVisibility()
    {
        loaded++;
        if(loaded==2)
        {
            loaded=0;
            loadingView.setVisibility(View.GONE);
            recycleViews.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IWallpaperClickListener) {
            listener = (IWallpaperClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IWallpaperClickListener");
        }
    }

    @Override
    public void onClickWallpaper(int id, int position) {
        listener.onClickWallpaper(adapters.get(id).getItem(position));
    }
}
