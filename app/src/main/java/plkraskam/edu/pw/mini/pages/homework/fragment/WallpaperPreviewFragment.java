package plkraskam.edu.pw.mini.pages.homework.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;

import mbanje.kurt.fabbutton.FabButton;
import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.exceptions.PlaylistNotEnabledException;
import plkraskam.edu.pw.mini.pages.homework.helpers.WallpaperPlaylist;
import plkraskam.edu.pw.mini.pages.homework.helpers.constants.Constants;
import plkraskam.edu.pw.mini.pages.homework.helpers.enums.ImageFilterType;
import plkraskam.edu.pw.mini.pages.homework.helpers.ImageFilters;
import plkraskam.edu.pw.mini.pages.homework.helpers.ImageSaver;
import plkraskam.edu.pw.mini.pages.homework.helpers.toasts.Toasts;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IApiEndpoint;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IWallpaperPreviewListener;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperFull;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;
import plkraskam.edu.pw.mini.pages.homework.storage.DbActions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static mbanje.kurt.fabbutton.R.drawable.ic_fab_complete;


public class WallpaperPreviewFragment extends Fragment {


    private WallpaperSingleItemModel _passedItem;
    private FabButton setButton;
    private Boolean isSingleWallpaperEnabled;
    private ArrayList<Pair<ImageButton, ImageFilterType>> filterButtons;
    private ImageFilterType filterType;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallpaper_preview, container, false);

        ImageSaver.verifyStoragePermissions(getActivity());

        InitiateViews(view);
        GetPassedItem(view);
        SetFilterButtons(filterType);

        return view;
    }

    private void GetPassedItem(View view) {
        Bundle args = getArguments();
        if (args != null) {
            WallpaperSingleItemModel passedWallpaper = args.getParcelable("wallpaper");
            if (passedWallpaper != null) {
                PutPassedItem(view, passedWallpaper);
                GenerateFilters(view, passedWallpaper);
            }
        }
    }

    private void InitiateViews(View view) {
        setButton = (FabButton) view.findViewById(R.id.determinate);
        SharedPreferences _preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        isSingleWallpaperEnabled = !_preferences.getBoolean(Constants.SETTING_PLAYLIST_ENABLED, Boolean.FALSE);

        if (!isSingleWallpaperEnabled) {
            setButton.setIcon(getResources().getDrawable(R.drawable.ic_add_white_24dp, getContext().getTheme()), getResources().getDrawable(ic_fab_complete, getContext().getTheme()));
            setButton.setIndeterminate(true);
        }

        filterType = ImageFilterType.NONE;
    }

    private void PutPassedItem(View view, WallpaperSingleItemModel item) {
        SimpleDraweeView backgroundImage = (SimpleDraweeView) view.findViewById(R.id.backgroundImage);
        _passedItem = item;


        Uri uri = Uri.parse(item.getUrlFull());
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setAutoPlayAnimations(true)
                .build();
        backgroundImage.setController(controller);

        TextView title = (TextView) view.findViewById(R.id.title);
        TextView subtitle = (TextView) view.findViewById(R.id.subtitle);
        TextView price = (TextView) view.findViewById(R.id.price);
        title.setText(item.getName());
        subtitle.setText(item.getDescription());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IApiEndpoint apiService = retrofit.create(IApiEndpoint.class);

        Call<WallpaperFull> freeWallpapers = apiService.getFullWallpaper(_passedItem.getID());
        freeWallpapers.enqueue(new Callback<WallpaperFull>() {
            @Override
            public void onResponse(Call<WallpaperFull> call, Response<WallpaperFull> response) {
                WallpaperFull paged = response.body();

                String val = getResources().getString(R.string.wpf_downloaded_x_times);
                Object[] args = new Object[]{paged.getDownloadCount()};
                MessageFormat fmt = new MessageFormat(val);
                price.setText(fmt.format(args));
            }

            @Override
            public void onFailure(Call<WallpaperFull> call, Throwable t) {
                Toasts.FailLoadingDataCheckConnection(getContext());
            }
        });


        //BUTTONS

        setButton.setOnClickListener(v -> {

            if (isSingleWallpaperEnabled) {
                ProcessSingleWallpaperAction(_passedItem);
            } else {
                ProcessPlaylistAddAction(_passedItem);
            }
        });
    }

    private void ProcessPlaylistAddAction(WallpaperSingleItemModel item) {
        setButton.showProgress(true);

        try {
            DbActions.WallpaperPlaylist.Add(getContext(), item);
            WallpaperPlaylist.addNewItemOnPlaylist(item);

        } catch (PlaylistNotEnabledException e) {
            Toasts.PlaylistNotEnabled(getContext());
        }

        Toasts.PlaylistSuccessfullyAdded(getContext());

        setButton.showProgress(false);
    }

    private void ProcessSingleWallpaperAction(WallpaperSingleItemModel item) {

        setButton.resetIcon();
        setButton.showShadow(false);
        setButton.showProgress(true);
        setButton.setProgress(0);

        DbActions.WallpaperOwned.CheckIfExistsAddIfNot(getContext(), item);

        ImageSaver.saveImage(item.getFileName(), item.getUrlFull(), getContext(), true, filterType, setButton, this.getActivity());
        //PixelAnimationsController.StartAnimation(getContext());
        //listener.CloseFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IWallpaperPreviewListener) {
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnItemAddFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (MalformedURLException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    private void GenerateFilters(View view, WallpaperSingleItemModel item) {
        filterButtons = new ArrayList<Pair<ImageButton, ImageFilterType>>();

        filterButtons.add(new Pair<>((ImageButton) view.findViewById(R.id.filter1), ImageFilterType.INVERTED_COLORS));
        filterButtons.add(new Pair<>((ImageButton) view.findViewById(R.id.filter2), ImageFilterType.BLACK_AND_WHITE));
        filterButtons.add(new Pair<>((ImageButton) view.findViewById(R.id.filter3), ImageFilterType.RED_CHANNEL));
        filterButtons.add(new Pair<>((ImageButton) view.findViewById(R.id.filter4), ImageFilterType.EDGE_DETECTION));
        filterButtons.add(new Pair<>((ImageButton) view.findViewById(R.id.filter5), ImageFilterType.NONE));

        for (Pair<ImageButton, ImageFilterType> p : filterButtons) {
            p.first.setOnClickListener(v ->
            {
                SetFilterButtons(p.second);
            });
        }

        new RetrieveBitmap().execute(item.getUrlStatic());
    }

    private void SetFilterButtons(ImageFilterType type) {


        Pair<ImageButton, ImageFilterType> current = null;
        Pair<ImageButton, ImageFilterType> selected = null;

        for (Pair<ImageButton, ImageFilterType> p : filterButtons) {
            if (p.second == filterType) current = p;
            if (p.second == type) selected = p;

        }
        current.first.setBackground(getResources().getDrawable(R.drawable.filter_button, getActivity().getTheme()));
        selected.first.setBackground(getResources().getDrawable(R.drawable.filter_button_selected, getActivity().getTheme()));

        filterType = type;

    }


    private class RetrieveBitmap extends AsyncTask<String, Void, Bitmap> {

        private Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);

            // "RECREATE" THE NEW BITMAP
            Bitmap resizedBitmap = Bitmap.createBitmap(
                    bm, 0, 0, width, height, matrix, false);
            bm.recycle();
            return resizedBitmap;
        }

        protected Bitmap doInBackground(String... url) {
            try {

                Bitmap downloaded = getBitmapFromURL(url[0]);
                return getResizedBitmap(downloaded, 100, 100);
            } catch (Exception e) {

                return null;
            }
        }

        protected void onPostExecute(Bitmap downloaded) {
            Bitmap f1 = ImageFilters.bitmapAfterImageEffect(downloaded, ImageFilterType.INVERTED_COLORS);
            Bitmap f2 = ImageFilters.bitmapAfterImageEffect(downloaded, ImageFilterType.BLACK_AND_WHITE);
            Bitmap f3 = ImageFilters.bitmapAfterImageEffect(downloaded, ImageFilterType.RED_CHANNEL);
            Bitmap f4 = ImageFilters.bitmapAfterImageEffect(downloaded, ImageFilterType.EDGE_DETECTION);
            BitmapDrawable f1d = new BitmapDrawable(getResources(), f1);
            BitmapDrawable f2d = new BitmapDrawable(getResources(), f2);
            BitmapDrawable f3d = new BitmapDrawable(getResources(), f3);
            BitmapDrawable f4d = new BitmapDrawable(getResources(), f4);
            BitmapDrawable f5d = new BitmapDrawable(getResources(), downloaded);

            filterButtons.get(0).first.setImageDrawable(f1d);
            filterButtons.get(1).first.setImageDrawable(f2d);
            filterButtons.get(2).first.setImageDrawable(f3d);
            filterButtons.get(3).first.setImageDrawable(f4d);
            filterButtons.get(4).first.setImageDrawable(f5d);
        }
    }

}
