package plkraskam.edu.pw.mini.pages.homework.helpers;

/**
 * Created by tweant on 30.04.2017.
 */

public class Flags {
    public static final int RESULT_OK_RETURN_PARENT = 10001;
    public static final int RESULT_GO_MAIN_FRAGMENT = 10101;
    public static final int REQUEST_CODE_PLAYLIST_EDIT = 20001;
    public static final int REQUEST_CODE_WALLPAPER_PREVIEW = 20002;
    public static final int REQUEST_CODE_WALLPAPER_UPLOAD_TEXT = 20003;
}
