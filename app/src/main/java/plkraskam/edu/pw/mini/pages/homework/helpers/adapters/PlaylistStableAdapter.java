package plkraskam.edu.pw.mini.pages.homework.helpers.adapters;

/*
 * Copyright 2014 Magnus Woxblom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.util.Pair;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;

import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.helpers.constants.Constants;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IPlaylistEditItemListener;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;

import static java.lang.Boolean.FALSE;

public class PlaylistStableAdapter extends DragItemAdapter<Pair<Long, WallpaperSingleItemModel>, PlaylistStableAdapter.ViewHolder> {

    private int mLayoutId;
    private int mGrabHandleId;
    private Context mContext;
    private boolean mDragOnLongPress;
    private static IPlaylistEditItemListener itemListener;

    public PlaylistStableAdapter(Context context, ArrayList<Pair<Long, WallpaperSingleItemModel>> list, int layoutId, int grabHandleId, boolean dragOnLongPress, IPlaylistEditItemListener listener) {
        mLayoutId = layoutId;
        mContext = context;
        mGrabHandleId = grabHandleId;
        mDragOnLongPress = dragOnLongPress;
        setHasStableIds(true);
        itemListener=listener;
        setItemList(list);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return new ViewHolder(view);
    }

    private Context getContext() {
        return mContext;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        WallpaperSingleItemModel singleItem = mItemList.get(position).second;
        holder.tvTitle.setText(singleItem.getName());

        holder.description.setText(singleItem.getDescription());

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        Boolean showDynamic = preferences.getBoolean(Constants.SETTING_WALLPAPER_SHOW_DYNAMIC_ENABLED,FALSE);
        if (showDynamic)
        {
            Glide.with(getContext())
                    .load(singleItem.getUrlGif())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .into(holder.itemImage);
        }
        else
        {
            Glide.with(getContext())
                    .load(singleItem.getUrlStatic())
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .into(holder.itemImage);
        }

        holder.buttonViewOption.setOnClickListener(view -> {

            PopupMenu popup = new PopupMenu(getContext(), holder.buttonViewOption);
            popup.inflate(R.menu.playlist_edit_options_menu);
            popup.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
//                    case R.id.editButtonMenu:
//                        itemListener.recyclerViewListEdit(view, holder.getLayoutPosition());
//                        break;
                    case R.id.removeButtonMenu:
                        itemListener.onRemoveItemClick(view, holder.getLayoutPosition());
                        break;
                }
                return false;
            });
            popup.show();

        });

        holder.itemView.setTag(mItemList.get(position));
    }

    @Override
    public long getItemId(int position) {
        return mItemList.get(position).first;
    }

    class ViewHolder extends DragItemAdapter.ViewHolder {

        TextView tvTitle;
        TextView description;
        TextView buttonViewOption;
        SimpleDraweeView itemImage;

        ViewHolder(final View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            this.description = (TextView) itemView.findViewById(R.id.tvDescription);
            this.itemImage = (SimpleDraweeView) itemView.findViewById(R.id.itemImage);
            this.buttonViewOption = (TextView) itemView.findViewById(R.id.textViewOptions);
        }

        @Override
        public void onItemClicked(View view) {
            Toast.makeText(view.getContext(), "Item clicked", Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean onItemLongClicked(View view) {
            Toast.makeText(view.getContext(), "Item long clicked", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

}
