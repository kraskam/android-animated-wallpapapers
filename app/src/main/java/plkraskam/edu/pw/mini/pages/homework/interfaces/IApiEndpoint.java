package plkraskam.edu.pw.mini.pages.homework.interfaces;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperFull;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperPaged;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by tweant on 04.05.2017.
 */

public interface IApiEndpoint {
    @GET("wallpapers/{id}")
    Call<WallpaperFull> getFullWallpaper(@Path("id") int id);

    @GET("wallpapers/")
    Call<WallpaperPaged> getWallpapers(@Query("pageNo") int pageNo, @Query("pageSize") int pageSize, @Query("filter") String filter, @Query("sort") String sort);

    @Multipart
    @POST("wallpapers/post")
    Call<ResponseBody> postImage(@Part MultipartBody.Part image, @Query("name") String name, @Query("description") String description);
}
