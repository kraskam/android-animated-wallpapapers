package plkraskam.edu.pw.mini.pages.homework.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import mbanje.kurt.fabbutton.FabButton;
import plkraskam.edu.pw.mini.pages.homework.helpers.enums.ImageFilterType;


/**
 * Created by Szprota on 03-May-17.
 */

public class ImageSaver extends  AsyncTask <Void,Void,SavingFile> {

    private static String fileName;
    private static String urlPath = "http://androidsaveitem.appspot.com/downloadjpg";
    private static Context context;

    private static boolean toWallpaper;
    private static ImageFilterType imageFilterType;

    //New
    private static FabButton setButton;
    private static Activity activity;
    private static float currentProgress = 0;
    private static long curBytes = 0;
    private static int bytesCount =0;

    /**
     *
     * @param fileName
     * @param url
     * @param context
     * @param wallpaper
     * @param ift
     * @param fabButton Button that sets image and displays progress.
     * @param activityFromFragment Activity taken from fragment
     */
    public static void saveImage(String fileName, String url, Context context, boolean wallpaper, ImageFilterType ift, FabButton fabButton, Activity activityFromFragment){
        ImageSaver.fileName = fileName;
        ImageSaver.urlPath = url;
        ImageSaver.context = context;
        toWallpaper = wallpaper;
        imageFilterType = ift;

        setButton=fabButton;
        activity=activityFromFragment;

        instance().execute();

    }

    public static void saveImage(String fileName, String url, Context context, boolean wallpaper){
        ImageSaver.fileName = fileName;
        ImageSaver.urlPath = url;
        ImageSaver.context = context;
        toWallpaper = wallpaper;
        imageFilterType = ImageFilterType.NONE;


        instance().execute();

    }

    private static ImageSaver instance(){
        return new ImageSaver();
    }

    @Override
    protected SavingFile doInBackground( Void... params ) {
        InputStream is = null;
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        boolean setAsWallpaper = toWallpaper;
        ImageFilterType ift = imageFilterType;
        File file = new File( path , fileName ) ;
        try {
            // Make sure the Pictures directory exists.path.mkdirs()
            path.mkdirs();

            URL url = new URL(urlPath);
            /* Open a connection to that URL. */
            URLConnection ucon = url.openConnection();

            is = ucon.getInputStream();

            OutputStream os = new FileOutputStream(file) ;

            int n;
            byte [ ] data = new byte [ 4096 ] ;
            curBytes = 0;
            bytesCount = ucon.getContentLength();
            while ((n = is.read(data)) != -1)
            {
                curBytes += n;

                //onProgressUpdate(curBytes/bytesCount);
                if(activity != null){
                    getRunnable(activity).run();
                }

                os.write(data, 0, n);
            }


            is.close ( ) ;
            os.close ( ) ;
            return new SavingFile(file,setAsWallpaper,ift);
        }
        catch (Exception e){
            Log .d ( "ImageSaver " , " Error: " + e ) ;
        }

        return null;
    }


    private Runnable getRunnable(final Activity activity){
        return new Runnable() {
            @Override
            public void run() {
                currentProgress=((float)curBytes)/bytesCount*100;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setButton.setProgress(currentProgress);
                    }
                });
            }
        };
    }

    protected void onPostExecute (SavingFile savingFile) {
        if(savingFile == null) {
            Toast toast = Toast.makeText(context,"Something went wrong", Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        if(savingFile.toWallpaper){
            WallpaperController.setWallpaper(savingFile.file,ImageSaver.context, savingFile.ift);

            PixelAnimationsController.StartAnimation(context);
            boolean deleted = savingFile.file.delete(); //deleting file afterwards
        }
        activity = null;
        setButton = null;
        try{
            MediaScannerConnection.scanFile( null , new String [] {savingFile.file.toString( ) } , null , new MediaScannerConnection.OnScanCompletedListener ( ) { public void onScanCompleted (String path, Uri uri) {
                Log.i ( " External Storage" , " Scanned " + path + " : " ) ;
            } } ) ;


        }catch (Exception e) {
            // TODO: handle exception
        }

    }




    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
