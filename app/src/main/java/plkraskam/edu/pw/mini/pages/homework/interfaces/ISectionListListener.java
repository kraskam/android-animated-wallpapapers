package plkraskam.edu.pw.mini.pages.homework.interfaces;

/**
 * Created by tweant on 25.04.2017.
 */

public interface ISectionListListener {
    void onClickWallpaper(int id, int position);
}
