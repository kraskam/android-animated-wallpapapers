package plkraskam.edu.pw.mini.pages.homework.helpers.constants;

/**
 * Created by tweant on 01.05.2017.
 */

public class Constants {
    public static final String SETTING_WALLPAPER_SHOW_DYNAMIC_ENABLED = "wallpapers_show_dynamic";
    public static final String SETTING_PLAYLIST_ENABLED = "setting_playlist_enabled";
    public static final String SETTING_PLAYLIST_RANDOM_ENABLED = "settings_playlist_random_enabled";
    public static final String SETTING_PLAYLIST_HOURS = "settings_playlist_hours";
    public static final String SETTING_PLAYLIST_MINUTES = "settings_playlist_minutes";
    public static final String SETTING_ = "";

    public static final String API_URL = "http://animatedwallpapers.azurewebsites.net/api/";

    public static final boolean LOGGING = false;

    /*
      Your imgur client id. You need this to upload to imgur.

      More here: https://api.imgur.com/
     */
    public static final String MY_IMGUR_CLIENT_ID = "1658c77a6ab8a3a";
    public static final String MY_IMGUR_CLIENT_SECRET = "1c56f9f36542912eec39ba037f28a074c7d770b4";

    /*
      Redirect URL for android.
     */
    public static final String MY_IMGUR_REDIRECT_URL = "http://android";

    /*
      Client Auth
     */
    public static String getClientAuth() {
        return "Client-ID " + MY_IMGUR_CLIENT_ID;
    }
}
