package plkraskam.edu.pw.mini.pages.homework.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import plkraskam.edu.pw.mini.pages.homework.R;
import plkraskam.edu.pw.mini.pages.homework.fragment.WallpaperPreviewFragment;
import plkraskam.edu.pw.mini.pages.homework.interfaces.IWallpaperPreviewListener;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class WallpaperPreviewActivity extends AppCompatActivity  implements IWallpaperPreviewListener {

    private FragmentManager _supportFragmentManager;
    private WallpaperPreviewFragment _wallpaperPreviewFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallpaper_preview);

        _supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = _supportFragmentManager.beginTransaction();
        _wallpaperPreviewFragment = new WallpaperPreviewFragment();
        WallpaperSingleItemModel passedWallpaper = getIntent().getParcelableExtra("wallpaper");
        if(passedWallpaper!=null)
        {
            Bundle args = new Bundle();
            args.putParcelable("wallpaper", passedWallpaper);
            _wallpaperPreviewFragment.setArguments(args);
        }

        fragmentTransaction.replace(R.id.activit_container, _wallpaperPreviewFragment, "ItemAddfragment");
        fragmentTransaction.commit();
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        this.CloseFragment();
    }

    @Override
    public void CloseFragment() {
        Intent returnIntent = NavUtils.getParentActivityIntent(this);
        returnIntent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        NavUtils.navigateUpTo(this, returnIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.CloseFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
