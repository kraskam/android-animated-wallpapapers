package plkraskam.edu.pw.mini.pages.homework.helpers.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.woxthebox.draglistview.DragItem;

import plkraskam.edu.pw.mini.pages.homework.R;

/**
 * Created by tweant on 14.05.2017.
 */

public class WallpaperPlaylistDragItem extends DragItem {

    private Context context;
    public WallpaperPlaylistDragItem(Context context, int layoutId) {
        super(context, layoutId);
        this.context = context;
    }

    @Override
    public void onBindDragView(View clickedView, View dragView) {
        CharSequence text = ((TextView) clickedView.findViewById(R.id.tvTitle)).getText();
        ((TextView) dragView.findViewById(R.id.tvTitle)).setText(text);
        CharSequence subtext = ((TextView) clickedView.findViewById(R.id.tvDescription)).getText();
        TextView textDescription = ((TextView) dragView.findViewById(R.id.tvDescription));
        textDescription.setText(subtext);
        SimpleDraweeView image = (SimpleDraweeView) clickedView.findViewById(R.id.itemImage);
        if (image != null) {
            Drawable drawable = image.getDrawable();
            if (drawable != null) {
                dragView.findViewById(R.id.itemImage).setBackground(drawable);
            }
        }

        CardView card = (CardView) dragView.findViewById(R.id.itemCard);
        CardView cardClicked = (CardView) clickedView.findViewById(R.id.itemCard);
        card.setRadius(cardClicked.getRadius());
        card.setCardElevation(cardClicked.getCardElevation());
        card.setCardBackgroundColor(ContextCompat.getColor(context,R.color.light_grey));

    }
}
