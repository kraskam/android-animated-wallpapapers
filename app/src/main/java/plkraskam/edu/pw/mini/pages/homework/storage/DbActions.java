package plkraskam.edu.pw.mini.pages.homework.storage;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.util.Pair;
import android.support.v7.preference.PreferenceManager;

import java.util.ArrayList;

import plkraskam.edu.pw.mini.pages.homework.exceptions.PlaylistNotEnabledException;
import plkraskam.edu.pw.mini.pages.homework.helpers.constants.Constants;
import plkraskam.edu.pw.mini.pages.homework.model.WallpaperSingleItemModel;

/**
 * Helper actions on manipulating database
 */
public class DbActions {

    public static class WallpaperPlaylist {

        /**
         * Add new wallpaper to user playlist previously checking if playlist functionality is enabled by user
         * @param context Context
         * @param wallpaper Wallpaper to add
         * @return The row ID of the newly inserted row, or -1 if an error occurred.
         */
        public static long Add(Context context, WallpaperSingleItemModel wallpaper) throws PlaylistNotEnabledException {
            if(CheckIfEnabled(context,Constants.SETTING_PLAYLIST_ENABLED, Boolean.FALSE))
            {
                WallpaperPlaylistDbHelper dbHelper = new WallpaperPlaylistDbHelper(context);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_ID, wallpaper.getID());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_NAME, wallpaper.getName());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DESCRIPTION, wallpaper.getDescription());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL, wallpaper.getUrlFull());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_GIF, wallpaper.getUrlGif());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_STATIC, wallpaper.getUrlStatic());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DOWNLOADED_COUNT, wallpaper.getDownloadCount());
                long newRowId = db.insert(WallpaperItemContract.WallpaperPlaylistEntry.TABLE_NAME, null, values);
                db.close();
                return newRowId;
            }
            else
            {
                throw new PlaylistNotEnabledException();
            }
        }

        /**
         * Loads "Wallpaper Playlist" table from database to list. Draggable version.
         * @param context Current context
         * @return "Wallpaper playlist" list in form of list of Pair(Long,WallpaperSingleItem)
         */
        public static ArrayList<Pair<Long,WallpaperSingleItemModel>> LoadListForDragging(Context context)
        {
            WallpaperPlaylistDbHelper dbHelper = new WallpaperPlaylistDbHelper(context);
            SQLiteDatabase db = dbHelper.getReadableDatabase();

            String[] projection = {
                    WallpaperItemContract.WallpaperPlaylistEntry._ID,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_ID,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_NAME,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DESCRIPTION,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_GIF,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_STATIC,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DOWNLOADED_COUNT
            };

            Cursor cursor = db.query(
                    WallpaperItemContract.WallpaperPlaylistEntry.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    null
            );

            ArrayList<Pair<Long,WallpaperSingleItemModel>> playlist = new ArrayList<>();
            long ind =0;
            while (cursor.moveToNext()) {
                int id = cursor.getInt(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_ID));
                String name = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_NAME));
                String description = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DESCRIPTION));
                String imageurl = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL));
                String imageurlgif = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_GIF));
                String imageurlstatic = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_STATIC));
                int downloadedCount = cursor.getInt(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DOWNLOADED_COUNT));
                playlist.add(new Pair<>(ind,new WallpaperSingleItemModel(id,name,description,downloadedCount, imageurlgif,imageurlstatic,imageurl)));
                ind++;
            }


            cursor.close();
            db.close();
            return playlist;
        }

        /**
         * Loads "Wallpaper Playlist" table from database to list.
         * @param context Current context
         * @return "Wallpaper playlist" list in form of list of WallpaperSingleItem
         */
        public static ArrayList<WallpaperSingleItemModel> LoadList(Context context) {
            WallpaperPlaylistDbHelper dbHelper = new WallpaperPlaylistDbHelper(context);
            SQLiteDatabase db = dbHelper.getReadableDatabase();

            String[] projection = {
                    WallpaperItemContract.WallpaperPlaylistEntry._ID,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_ID,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_NAME,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DESCRIPTION,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_GIF,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_STATIC,
                    WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DOWNLOADED_COUNT
            };

            Cursor cursor = db.query(
                    WallpaperItemContract.WallpaperPlaylistEntry.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    null
            );


            ArrayList<WallpaperSingleItemModel> playlist = new ArrayList<>();
            while (cursor.moveToNext()) {
                int id = cursor.getInt(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_ID));
                String name = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_NAME));
                String description = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DESCRIPTION));
                String imageurl = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL));
                String imageurlgif = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_GIF));
                String imageurlstatic = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_STATIC));
                int downloadedCount = cursor.getInt(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DOWNLOADED_COUNT));
                playlist.add(new WallpaperSingleItemModel(id,name,description,downloadedCount,  imageurlgif,imageurlstatic,imageurl));
            }

            cursor.close();
            db.close();
            return playlist;
        }

        /**
         * Saves "Wallpaper playlist" from list to databse.
         * @param context Current context
         * @param playlist Playlist to save
         */
        public static void SaveList(Context context, ArrayList<Pair<Long,WallpaperSingleItemModel>> playlist)
        {
            WallpaperPlaylistDbHelper dbHelper = new WallpaperPlaylistDbHelper(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(WallpaperItemContract.WallpaperPlaylistEntry.TABLE_NAME, null, null);
            ContentValues values;
            for (Pair<Long,WallpaperSingleItemModel> item : playlist) {
                values = new ContentValues();
                WallpaperSingleItemModel wallpaper = item.second;
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_NAME, wallpaper.getID());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_NAME, wallpaper.getName());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DESCRIPTION, wallpaper.getDescription());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL, wallpaper.getUrlFull());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_GIF, wallpaper.getUrlGif());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_IMAGEURL_SMALL_STATIC, wallpaper.getUrlStatic());
                values.put(WallpaperItemContract.WallpaperPlaylistEntry.COLUMN_DOWNLOADED_COUNT, wallpaper.getDownloadCount());
                db.insert(WallpaperItemContract.WallpaperPlaylistEntry.TABLE_NAME, null, values);
            }

            db.close();
        }
    }

    public static class WallpaperOwned {

        /**
         * Checks if passed wallpaper exists already in the database, adds if not. Unselects all wallpapers and selects the passed one.
         * @param context Current context
         * @param item Wallpaper
         * @return The row ID of the newly inserted row, or -1 if an error occurred.
         */
        public static long CheckIfExistsAddIfNot(Context context, WallpaperSingleItemModel item)
        {
            long newRowId =-1;
            WallpaperOwnedDbHelper dbHelper = new WallpaperOwnedDbHelper(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String ind = String.valueOf(item.getID());
            Cursor cursor = db.query(true, WallpaperItemContract.WallpaperOwnedEntry.TABLE_NAME,
                    null,
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_ID + "=?",
                    new String[]{ind},
                    null,
                    null,
                    null,
                    null);

            ContentValues unselect = new ContentValues();
            unselect.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_SELECTED, 0);
            db.update(WallpaperItemContract.WallpaperOwnedEntry.TABLE_NAME, unselect, null, null);

            if (cursor.getCount() == 1) {
                cursor.moveToFirst();
                String id = String.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperOwnedEntry._ID)));
                ContentValues cv = new ContentValues();
                cv.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_SELECTED, 1);
                db.update(WallpaperItemContract.WallpaperOwnedEntry.TABLE_NAME, cv, "_id=?", new String[]{id});
            } else {


                ContentValues values = new ContentValues();
                values.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_ID, item.getID());
                values.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_SELECTED, 1);
                values.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_NAME, item.getName());
                values.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_DESCRIPTION, item.getDescription());
                values.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL, item.getUrlFull());
                values.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL_SMALL_GIF, item.getUrlGif());
                values.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL_SMALL_STATIC, item.getUrlStatic());
                values.put(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_DOWNLOADED_COUNT, item.getDownloadCount() + 1);
                newRowId = db.insert(WallpaperItemContract.WallpaperOwnedEntry.TABLE_NAME, null, values);
            }
            db.close();
            cursor.close();
            return newRowId;
        }

        /**
         * Loads "Wallpaper Owned" table to list from database.
         * @param context Current context
         * @return "Wallpaper owned" list in form of list of WallpaperSingleItem
         */
        public static ArrayList<WallpaperSingleItemModel> LoadList(Context context) {
            WallpaperOwnedDbHelper dbHelper = new WallpaperOwnedDbHelper(context);
            SQLiteDatabase db = dbHelper.getReadableDatabase();

            String[] projection = {
                    WallpaperItemContract.WallpaperOwnedEntry._ID,
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_ID,
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_SELECTED,
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_NAME,
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_DESCRIPTION,
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL,
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL_SMALL_GIF,
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL_SMALL_STATIC,
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_DOWNLOADED_COUNT
            };

            Cursor cursor = db.query(
                    WallpaperItemContract.WallpaperOwnedEntry.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    null
            );


            ArrayList<WallpaperSingleItemModel> list = new ArrayList<>();
            while (cursor.moveToNext()) {
                int id = cursor.getInt(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_ID));
                Boolean selected = 1 == cursor.getInt(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_SELECTED));
                String name = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_NAME));
                String description = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_DESCRIPTION));
                String imageurl = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL));
                String imageurlgif = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL_SMALL_GIF));
                String imageurlstatic = cursor.getString(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL_SMALL_STATIC));
                int downloadedCount = cursor.getInt(
                        cursor.getColumnIndexOrThrow(WallpaperItemContract.WallpaperOwnedEntry.COLUMN_DOWNLOADED_COUNT));
                WallpaperSingleItemModel wallpaper = new WallpaperSingleItemModel(id, name, description, downloadedCount, imageurlgif, imageurlstatic, imageurl);
                if (selected) wallpaper.setSelected();
                list.add(wallpaper);
            }
            cursor.close();
            db.close();
            return list;
        }
    }

    /**
     * Checks is setting is enabled by user.
     * @param context Current context
     * @param key Key from default settings to check
     * @param defaultValue Default value is setting was not set before
     * @return User saved setting
     */
    private static Boolean CheckIfEnabled(Context context, String key, Boolean defaultValue)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key,defaultValue);
    }

}
