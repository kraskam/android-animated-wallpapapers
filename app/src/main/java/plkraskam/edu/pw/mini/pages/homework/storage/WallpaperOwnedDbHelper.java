package plkraskam.edu.pw.mini.pages.homework.storage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by tweant on 03.05.2017.
 */

public class WallpaperOwnedDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "WallpaperOwned.db";

    public WallpaperOwnedDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + WallpaperItemContract.WallpaperOwnedEntry.TABLE_NAME + " (" +
                    WallpaperItemContract.WallpaperOwnedEntry._ID + " INTEGER PRIMARY KEY," +
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_ID + " INTEGER," +
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_SELECTED + " INTEGER," +
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_NAME + " TEXT," +
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_DESCRIPTION + " TEXT," +
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL + " TEXT,"+
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL_SMALL_GIF + " TEXT,"+
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_IMAGEURL_SMALL_STATIC + " TEXT,"+
                    WallpaperItemContract.WallpaperOwnedEntry.COLUMN_DOWNLOADED_COUNT + " INTEGER)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + WallpaperItemContract.WallpaperOwnedEntry.TABLE_NAME;


}
